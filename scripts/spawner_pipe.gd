# script: spawner_pipe

extends Node2D

const scn_pipe = preload("res://scenes/pipe.tscn")
const GROUND_HEIGHT = 56
const PIPE_WIDTH = 26
const OFFSET_X = 65
const OFFSET_Y = 55
const AMOUNT_TO_FILL_VIEW = 3

func _ready():
	var bird = utils.get_main_node().get_node("bird")
	
	if bird:
		bird.connect("state_changed", self, "_on_bird_state_changed", [], CONNECT_ONESHOT)

func _on_bird_state_changed(bird):
	if bird.get_state() == bird.STATE_FLAPPING:
		start()



func start():
	go_init_position()
	
	for i in range(AMOUNT_TO_FILL_VIEW):
		spawm_and_move()

func go_init_position():
	randomize()

	var init_position = Vector2()
	var view_size = get_viewport_rect().size

	init_position.x = view_size.x + PIPE_WIDTH / 2
	init_position.y = rand_range(0 + OFFSET_Y, view_size.y - GROUND_HEIGHT - OFFSET_Y)

	var camera = utils.get_main_node().get_node("camera")
	if camera:
		init_position.x += camera.get_total_position().x

	set_position(init_position)

func spawm_and_move():
	spawm_pipe()
	go_next_position()

func spawm_pipe():
	var new_pipe = scn_pipe.instance()
	
	new_pipe.set_position(position)
	new_pipe.connect("tree_exited", self, "spawm_and_move")
	get_node("container").add_child(new_pipe)

func go_next_position():
	randomize()
	
	var next_position = position
	var view_size = get_viewport_rect().size
	
	next_position.x += PIPE_WIDTH / 2 + OFFSET_X + PIPE_WIDTH / 2
	next_position.y = rand_range(0 + OFFSET_Y, view_size.y - GROUND_HEIGHT - OFFSET_Y)
	set_position(next_position)