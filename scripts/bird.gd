# script: bird

extends RigidBody2D

onready var state = FlyingState.new(self)
#onready var state = FlappingState.new(self) 

var speed = 50
var prev_state

const STATE_FLYING = 0
const STATE_FLAPPING = 1
const STATE_HIT = 2
const STATE_GROUNDED = 3

signal state_changed

func _ready():
	set_process_input(true);
	set_process_unhandled_input(true)
	set_physics_process(true)
	
	add_to_group(game.GROUP_BIRDS)
	connect("body_entered", self, "_on_body_enter")

func _physics_process(delta):
	state.update(delta)

func _input(event):
	state.input(event)

#func _unhandled_input(event):
#	if state.has_method("unhandled_input"):
#		state.unhandled_input(event)

func _on_body_enter(other_body):
	if state.has_method("on_body_enter"):
		state.on_body_enter(other_body)

func set_state(new_state):
	state.exit()
	prev_state = get_state()

	if new_state == STATE_FLYING:
		state = FlyingState.new(self)
	elif new_state == STATE_FLAPPING:
		state = FlappingState.new(self)
	elif new_state == STATE_HIT:
		state = HitState.new(self)
	elif new_state == STATE_GROUNDED:
		state = GroundedState.new(self)
	emit_signal("state_changed", self)

func get_state():
	if state is FlyingState:
		return STATE_FLYING
	elif state is FlappingState:
		return STATE_FLAPPING
	elif state is HitState:
		return STATE_HIT
	elif state is GroundedState:
		return STATE_GROUNDED


# class FlyingState -----------------------------------------------------
class FlyingState:
	var bird 
	var prev_gravity_scale

	func _init(bird):
		self.bird = bird
		bird.get_node("anim").play("flying")
		bird.set_linear_velocity(Vector2(bird.speed, bird.get_linear_velocity().y))
		
		prev_gravity_scale = bird.get_gravity_scale()
		bird.set_gravity_scale(0)
	
	func update(delta):
		pass
	
	func input(event):
		pass
	
	func exit():
		bird.set_gravity_scale(prev_gravity_scale)
		bird.get_node("anim").stop()
		bird.get_node("anim_sprite").set_position(Vector2(0, 0))


# class FlappingState ---------------------------------------------------
class FlappingState:
	var bird 
	
	func _init(bird):
		self.bird = bird
		
		bird.set_linear_velocity(Vector2(bird.speed, bird.get_linear_velocity().y))
		flap()

	func update(delta):
		# if roation of bird is less then -30
		if rad2deg(bird.rotation) < -30:
			bird.set_angular_velocity(0)
		else:
			bird.set_angular_velocity(-1.5)

		# if bird is facing down
		if bird.get_linear_velocity().y > 0:
			bird.set_angular_velocity(1.5)

	#func unhandled_input(event):
	#	if !InputEventMouseButton: 
	#		return
	#	
	#	if InputEventMouseButton.is_pressed():
	#		flap()

	func on_body_enter(other_body):
		if other_body.is_in_group(game.GROUP_PIPES):
			bird.set_state(bird.STATE_HIT)
		elif other_body.is_in_group(game.GROUP_GROUNDS):
			bird.set_state(bird.STATE_GROUNDED)

	func flap():
		bird.set_linear_velocity(Vector2(bird.get_linear_velocity().x, -150))
		bird.set_angular_velocity(-3)
		bird.get_node("anim").play("flap")
		
		audio_player.get_node("aud_wing").play()

	func input(event):
		if event.is_action_pressed("flap"):
			flap()
		elif event.is_pressed() and event.button_index == BUTTON_LEFT:
			flap()

	func exit():
		pass

# class HitState --------------------------------------------------------
class HitState:
	var bird 
	
	func _init(bird):
		self.bird = bird
		
		bird.set_linear_velocity(Vector2(0, 0))
		bird.set_angular_velocity(2)
		
		var other_body = bird.get_colliding_bodies()[0]
		bird.add_collision_exception_with(other_body)
		
		audio_player.get_node("aud_hit").play()
		audio_player.get_node("aud_die").play()
	
	func update(delta):
		pass
	
	func input(event):
		pass
	
	func on_body_enter(other_body):
		if other_body.is_in_group(game.GROUP_GROUNDS):
			bird.set_state(bird.STATE_GROUNDED)

	func exit():
		pass

# class GroundedState ---------------------------------------------------
class GroundedState:
	var bird 
	
	func _init(bird):
		self.bird = bird
		
		bird.set_linear_velocity(Vector2(0, 0))
		bird.set_angular_velocity(0)
		
		if bird.prev_state != bird.STATE_HIT:
			audio_player.get_node("aud_hit").play()
	
	func update(delta):
		pass
	
	func input(event):
		pass
	
	func exit():
		pass