# script camera

extends Camera2D

onready var bird = utils.get_main_node().get_node("bird")

func _ready():
	set_physics_process(true)
	
func _physics_process(delta):
	set_position(Vector2(bird.position.x, position.y))

func get_total_position():
	return position + offset