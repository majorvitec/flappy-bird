# Flappy Bird [2018]

Created a Flappy Bird game [Followed Tutorial].

## Playable Example
[Play Flappy Bird](https://majorvitec.itch.io/flappy-bird)

## Game Overview

### Start Screen

![Start Screen](/images/readme/starting_screen.png "Start Screen")

### Playing Screen

![Playing Screen](/images/readme/playing_screen.png "Playing Screen")

### Try Again Screen

![Try Again Screen](/images/readme/play_again_screen.png "Try Again Screen")
